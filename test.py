import time
import RPi.GPIO as GPIO
import curses
import cv2


cap=cv2.VideoCapture(0)
screen = curses.initscr()
curses.noecho()
curses.cbreak()
screen.keypad(True)


GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)
GPIO.setup(16,GPIO.OUT)  #moteur gauche arriere
GPIO.setup(11,GPIO.OUT) #moteur gauche avant
GPIO.setup(13,GPIO.OUT) #moteur droit avant
GPIO.setup(15,GPIO.OUT) #moteur droit arriere
GPIO.setup(32,GPIO.OUT) #pwm moteur gauche
GPIO.setup(33,GPIO.OUT) #pwm moteur droit

l=GPIO.PWM(32,400)          #l=left 
l.start(100)

r=GPIO.PWM(33,400)          #r=right
r.start(100)

try :
    while True :
        
        ret, frame = cap.read()
        char = screen.getch()
        cv2.imshow("frame",frame)
       
        if char == ord('q'):
            break
        
        elif char == curses.KEY_DOWN:
            print("backward")
            GPIO.output(16,False)
            GPIO.output(11,True)
            GPIO.output(13,True)
            GPIO.output(15,False)

        elif char == curses.KEY_UP:
            print("forward")
            GPIO.output(16,True)
            GPIO.output(11,False)
            GPIO.output(13,False)
            GPIO.output(15,True)

        elif char == curses.KEY_RIGHT:
            print("right")
            GPIO.output(16,False)
            GPIO.output(11,True)
            GPIO.output(13,False)
            GPIO.output(15,True)

        elif char == curses.KEY_LEFT:
            print("left")
            GPIO.output(16,True)
            GPIO.output(11,False)
            GPIO.output(13,True)
            GPIO.output(15,False)
            
        elif char == 0:
            print ("stop")
            GPIO.output(16,False)
            GPIO.output(11,False)
            GPIO.output(13,False)
            GPIO.output(15,False)

    

finally:
        cap.release()
        cv2.destroyAllWindows()
        curses.nocbreak()
        screen.keypad(0)
        curses.echo()
        curses.endwin()
        l.stop()
        r.stop()
        GPIO.cleanup()


