import numpy as np
import cv2

cap = cv2.VideoCapture(0)
list = []


while (True):
    ret, frame = cap.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(gray, (5, 5), 0)
    canny = cv2.Canny(blur, 50, 100)
    lines = cv2.HoughLinesP(canny, 1, np.pi / 180, 50, maxLineGap=50)


    if lines is not None:
         for line in lines:
             x1, y1, x2, y2 = line[0]
             cv2.line(frame, (x1, y1), (x2, y2), (0, 255, 0), 5)
    del list[:]
    list[:] = []
    for y in range(239, 479):
        for x in range(289, 349):
            if np.array_equal(frame[y][x], [0,255,0]):
                list.append(y)
                list.sort()

    if len(list) == 0:
        break
    else:        
        cv2.line(frame, (0, list[-1]), (639, list[-1]), (255, 0, 0), 5)
        distance = (479-(list[-1]))*1,23
        print(distance)

    cv2.imshow("test",frame)



    
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
