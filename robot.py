import time
import RPi.GPIO as GPIO
import numpy as np
import cv2

#-----------SETUP-------------------------------
cap = cv2.VideoCapture(0)
list = []

GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)
GPIO.setup(16,GPIO.OUT)  #moteur gauche arriere
GPIO.setup(11,GPIO.OUT) #moteur gauche avant
GPIO.setup(13,GPIO.OUT) #moteur droit avant
GPIO.setup(15,GPIO.OUT) #moteur droit arriere
GPIO.setup(32,GPIO.OUT) #pwm moteur gauche
GPIO.setup(33,GPIO.OUT) #pwm moteur droit
GPIO.output(16,True)
GPIO.output(11,False)
GPIO.output(13,False)
GPIO.output(15,True)

#set GPIO Pins (capteur droit)
GPIO_TRIGGER = 18 #capteur droit trig pin
GPIO_ECHO = 22    #capteur droit echo pin
 
#set GPIO direction (IN / OUT) (capteur droit)
GPIO.setup(GPIO_TRIGGER, GPIO.OUT)
GPIO.setup(GPIO_ECHO, GPIO.IN)

#set GPIO Pins (capteur gauche)
GPIO_TRIGGER1 = 37 #capteur gauche trig pin
GPIO_ECHO1 = 36    #capteur gauche echo pin
 
#set GPIO direction (IN / OUT) (capteur avant)
GPIO.setup(GPIO_TRIGGER1, GPIO.OUT)
GPIO.setup(GPIO_ECHO1, GPIO.IN)


l=GPIO.PWM(32,200)          #l=left 
l.start(90)

r=GPIO.PWM(33,200)          #r=right
r.start(90)

#------------------------MAIN------------------------------------------

#distance (capteur droit) calcul :

def distance():
    # set Trigger to HIGH
    GPIO.output(GPIO_TRIGGER, True)
 
    # set Trigger after 0.01ms to LOW
    time.sleep(0.00001)
    GPIO.output(GPIO_TRIGGER, False)
 
    StartTime = time.time()
    StopTime = time.time()
 
    # save StartTime
    while GPIO.input(GPIO_ECHO) == 0:
        StartTime = time.time()
 
    # save time of arrival
    while GPIO.input(GPIO_ECHO) == 1:
        StopTime = time.time()
 
    # time difference between start and arrival
    TimeElapsed = StopTime - StartTime
    distance = (TimeElapsed * 34300) / 2
 
    return distance #in cm


#distance capteur gauche calcul :

def distance1():
    # set Trigger to HIGH
    GPIO.output(GPIO_TRIGGER1, True)
 
    # set Trigger after 0.01ms to LOW
    time.sleep(0.00001)
    GPIO.output(GPIO_TRIGGER1, False)
 
    StartTime1 = time.time()
    StopTime1 = time.time()
 
    # save StartTime
    while GPIO.input(GPIO_ECHO1) == 0:
        StartTime1 = time.time()
 
    # save time of arrival
    while GPIO.input(GPIO_ECHO1) == 1:
        StopTime1 = time.time()
 
    # time difference between start and arrival
    TimeElapsed1 = StopTime1 - StartTime1
    distance1 = (TimeElapsed1 * 34300) / 2
 
    return distance1 #in cm

if __name__ == '__main__':

    
    while True:
    ret, frame = cap.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(gray, (5, 5), 0)
    canny = cv2.Canny(blur, 50, 100)
    lines = cv2.HoughLinesP(canny, 1, np.pi / 180, 50, maxLineGap=50)
        if lines is not None:
         for line in lines:
             x1, y1, x2, y2 = line[0]
             cv2.line(frame, (x1, y1), (x2, y2), (0, 255, 0), 5)
    del list[:]
    list[:] = []
    for y in range(239, 479):
        for x in range(289, 349):
            if np.array_equal(frame[y][x], [0,255,0]):
                list.append(y)
                list.sort()

    if len(list) == 0:
        break
    else:        
        cv2.line(frame, (0, list[-1]), (639, list[-1]), (255, 0, 0), 5)
        distance2 = (479-(list[-1]))*1,23
        print(distance)
        
    if distance2>20:


        GPIO.output(16,True)
        GPIO.output(11,False)
        GPIO.output(13,False)
        GPIO.output(15,True)
        dist = distance()
        dist1 = distance1()
        
         if dist1>15:
            if dist<10:
                r.ChangeDutyCycle(100)
                l.ChangeDutyCycle(80)                
            elif dist>15:
                l.ChangeDutyCycle(100)
                r.ChangeDutyCycle(80)         
            else:
                r.ChangeDutyCycle(90)               
                l.ChangeDutyCycle(90)            
         else:
                r.ChangeDutyCycle(0)
                l.ChangeDutyCycle(0)
             

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()
GPIO.cleanup()
